import spark.servlet.SparkApplication;

import static spark.Spark.get;

public class HelloWorld implements SparkApplication {
	public static void main(String[] args) {
		new HelloWorld().init();
	}

	@Override
	public void init() {
		get("/hello", (req, res) -> "<font color=\"black\">THI IS HEADER!</font> \n <h1>This is Krishna, the Future DevOps Architect! Be positive</h1> \n <h2> Have a nice day </h2>");
	}
}
